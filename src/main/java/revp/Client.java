package revp;

/**
 * Created by pavelsilin on 1/24/16.
 */
public class Client {
    public static void main(String[] args) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            stringBuilder.append(args[i]);
        }
        RestrictionSitesLocator restrictionSitesLocator = new RestrictionSitesLocator(stringBuilder.toString());
        restrictionSitesLocator.findRestrictionSites();
        for (int i = 0; i < restrictionSitesLocator.coordAndLenOfRestrSites.size(); i++) {
            System.out.println(restrictionSitesLocator.coordAndLenOfRestrSites.get(i));
        }
    }
}
