package revp;

import util.Util;

import java.util.ArrayList;

/**
 * Created by pavelsilin on 1/24/16.
 */
public class RestrictionSitesLocator {

    String seq;
    ArrayList<String> coordAndLenOfRestrSites;
    int minLenRestrSites;
    int maxLenRestrSites;

    public RestrictionSitesLocator(String seq){
        this.seq = seq;
        coordAndLenOfRestrSites = new ArrayList<>();
        minLenRestrSites = 4;
        maxLenRestrSites = 12;
    }

    public void findRestrictionSites(){
        for (int i = 0; i < seq.length(); i++) {
            int currentMaxLenOfSite = maxLenRestrSites < seq.length() - i ? maxLenRestrSites : seq.length() - i;
            for (int j = currentMaxLenOfSite; j >= minLenRestrSites; j--) {
                String currentSubstring = seq.substring(i, i + j);
                String compStr = Util.complementsSeq(currentSubstring);
                if (Util.isIdenticalSeq(compStr, currentSubstring)){
                    coordAndLenOfRestrSites.add((i + 1) + " " + j);
                }
            }
        }
    }

}
