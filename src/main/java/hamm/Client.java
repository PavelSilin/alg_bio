package hamm;

import util.Util;

/**
 * Created by pavelsilin on 1/24/16.
 */
public class Client {
    public static void main(String[] args) {
        System.out.println(Util.hammingDistance(args[0], args[1]));
    }
}
