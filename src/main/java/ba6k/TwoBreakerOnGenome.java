package ba6k;

import ba6j.TwoBreakerOnGenomeGraph;
import util.Edge;

import java.util.*;

/**
 * @author Pavel Silin, silinpavel94@gmail.com
 */
public class TwoBreakerOnGenome {

    private List<Integer> genome;
    private Map<Integer, Edge> blackEdgesMap;
    private Map<Integer, Edge> colourEdgesMap;
    private List<List<Integer>> brokenGenomes;

    public TwoBreakerOnGenome(List<Integer> genome) {
        this.genome = genomeToGraph(genome);
        colourEdgesMap = new HashMap<>(genome.size() / 2);
        blackEdgesMap = new HashMap<>(genome.size() / 2);
        constructBlackAndColourEdges();
        this.brokenGenomes = new ArrayList<>();
    }

    public List<List<Integer>> getBrokenGenomes() {
        return brokenGenomes;
    }

    private List<Integer> genomeToGraph(List<Integer> genome) {
        List<Integer> graph = new ArrayList<>(genome.size() * 2);
        for (Integer edge: genome) {
            edgeToNodes(graph, edge);
        }
        return graph;
    }

    private void edgeToNodes(List<Integer> graph, Integer edge) {
        edge = edge * 2;
        if (edge < 0) {
            graph.add(Math.abs(edge));
            graph.add(Math.abs(edge + 1));
        } else {
            graph.add(edge - 1);
            graph.add(edge);
        }
    }

    private void constructBlackAndColourEdges() {
        for (int i = 0; i < genome.size() - 2; i++) {
            addEdgeToGraph(blackEdgesMap, i, i + 1);
            i++;
            addEdgeToGraph(colourEdgesMap, i, i + 1);
        }
        addEdgeToGraph(blackEdgesMap, genome.size() - 2, genome.size() - 1);
        addEdgeToGraph(colourEdgesMap, genome.size() - 1, 0);
    }

    private void addEdgeToGraph(Map<Integer, Edge> graph, int i, int j) {
        Integer firstNode = genome.get(i);
        Integer secondNode = genome.get(j);
        graph.put(firstNode, new Edge(Math.abs(firstNode), Math.abs(secondNode)));
    }

    public void constructBrokenGenomes() {
        List<Integer> brokenGenome = new ArrayList<>();
        Edge blackEdge = blackEdgesMap.values().iterator().next();
        blackEdgesMap.remove(blackEdge);

        while (!blackEdgesMap.isEmpty()){
            brokenGenome.add(blackEdge.getFrom());
            Edge colourEdge = colourEdgesMap.remove(blackEdge.getTo());

            if (colourEdge == null){
                brokenGenomes.add(brokenGenome);
                brokenGenome = new ArrayList<>();
                blackEdge = blackEdgesMap.values().iterator().next();
                continue;
            }

            brokenGenome.add(colourEdge.getFrom());
            blackEdge = blackEdgesMap.remove(colourEdge.getTo());
        }
        brokenGenomes.add(brokenGenome);
    }

    public void doTwoBreak(int i1, int i2, int j1, int j2) {
        List<Integer> indices = getIndices(i1, i2, j1, j2);
        colourEdgesMap = breakToGenomeGraph(indices);
    }

    private Map<Integer, Edge> breakToGenomeGraph(List<Integer> indices) {
        TwoBreakerOnGenomeGraph breakerOnGenomeGraph = new TwoBreakerOnGenomeGraph(colourEdgesMap.values());
        breakerOnGenomeGraph.doTwoBreak(indices);
        return breakerOnGenomeGraph.getColourMap();
    }

    private List<Integer> getIndices(int i1, int i2, int j1, int j2) {
        List<Integer> indices = new ArrayList<>();
        indices.add(i1);
        indices.add(i2);
        indices.add(j1);
        indices.add(j2);
        return indices;
    }
}
