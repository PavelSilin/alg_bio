package ba1e;

import java.awt.image.AreaAveragingScaleFilter;
import java.util.*;

/**
 * Created by pavelsilin on 1/15/16.
 */
public class FinderClumps {

    private String dna;
    private int kmerLen;
    private int clumpsSize;
    private Map<String, Integer> clumps;
    private ArrayList<String> answer;

    public FinderClumps( String dna,int kmerLen, int clumpsSize){
        this.dna = dna;
        this.kmerLen = kmerLen;
        this.clumpsSize = clumpsSize;
        this.clumps = new HashMap<>();
        this.answer = new ArrayList<>();

        findClumps();
    }

    private void findClumps() {
        findAllSizeClumps();
        selectAppropriateClumps();
    }

    private void findAllSizeClumps() {
        for (int i = 0; i < dna.length() - kmerLen; i++) {
            String currentKmer = dna.substring(i, i + kmerLen);
            Integer count = clumps.get(currentKmer);
            if (count == null){
                clumps.put(currentKmer, 1);
            }else{
                clumps.put(currentKmer, count + 1);
            }
        }
    }

    private void selectAppropriateClumps() {
        Set<Map.Entry<String, Integer>> clumpsSet = clumps.entrySet();
        for (Map.Entry<String, Integer> entry: clumpsSet){
            if (entry.getValue() >= clumpsSize){
                answer.add(entry.getKey());
            }
        }
    }

    public ArrayList<String> getAnswer(){
        return answer;
    }

}
