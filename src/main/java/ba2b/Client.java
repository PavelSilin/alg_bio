package ba2b;

/**
 * Created by pavelsilin on 1/15/16.
 */
public class Client {

    public static void main(String[] args) {

        int kmerLen = Integer.valueOf(args[0]);
        int sizeData = args.length - 1;
        String[] data = new String[sizeData];

        for (int i = 0; i < sizeData; i++) {
            data[i] = args[i + 1];
        }

        MedianFinder medianFinder = new MedianFinder(data, kmerLen);
        System.out.println("Median string is: " + medianFinder.getAnswer());
    }

}
