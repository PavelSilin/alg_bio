package ba2b;

import util.Util;

/**
 * Created by pavelsilin on 1/15/16.
 */
public class MedianFinder {

    private String[] dna;
    private int kmerLength;
    private int seqLen;
    private int minDistance;
    private String answer;

    public MedianFinder(String[] dna, int kmerLenght){
        this.dna = dna;
        this.answer = null;
        this.minDistance = Integer.MAX_VALUE;
        this.seqLen = dna[0].length();
        this.kmerLength = kmerLenght;

        findMedianString();
    }

    private void findMedianString(){
        for (int i = 0; i < dna.length; i++) {
            for (int j = 0; j < seqLen - kmerLength; j++){
                String currentKmer = dna[i].substring(j, j + kmerLength);
                int valueOfDistanceForThisKmer = distanceForThisKmer(i, currentKmer);

                if (valueOfDistanceForThisKmer < minDistance){
                    minDistance = valueOfDistanceForThisKmer;
                    answer = currentKmer;
                    if (minDistance == 0){
                        return;
                    }
                }
            }
        }
    }

    private int distanceForThisKmer(int indexOfSeqWithKmer, String currentKmer) {
        int distance = 0;
        for (int i = 0; i < dna.length; i++) {
            // distance between kmer and string where this kmer is locate = 0
            if (i == indexOfSeqWithKmer){
                continue;
            }
            distance += distanceForCurrentString(currentKmer, dna[i]);
        }
        return distance;
    }

    private int distanceForCurrentString(String currentKmer, String currentString) {
        int minDistanceForString = Integer.MAX_VALUE;
        for (int i = 0; i < seqLen - kmerLength; i++) {
            int localDistance = Util.hammingDistance(currentKmer, currentString.substring(i, i + kmerLength));
            if (minDistanceForString > localDistance){
                minDistanceForString = localDistance;
                if (minDistanceForString == 0){
                    break;
                }
            }
        }
        return minDistanceForString;
    }

    public String getAnswer() {
        return answer;
    }
}
