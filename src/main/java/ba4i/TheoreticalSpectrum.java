package ba4i;

import java.util.*;

/**
 * @author Anna Osipova Anna_Osipova@epam.com
 */

public class TheoreticalSpectrum {
    private static Map<Character, Integer> weights = Amino.aminoMassesMap();

    private static List<String> combine(String peptide){
        List<String> subpeptide = new ArrayList<>();
        for (int i = 0; i < peptide.length(); i++){
            for (int j = i; j < peptide.length(); j++) {
                subpeptide.add(peptide.substring(i, j + 1));
            }
        }

        for (int i = 2; i < peptide.length(); i++) {
            for (int j = 0; j < i - 1; j++) {
                subpeptide.add(peptide.substring(i,peptide.length()) + peptide.substring(0, j + 1));
            }
        }
        return subpeptide;
    }

    public static List<Integer> cyclospectrum(String peptide){
        List<Integer> mass = new ArrayList<>();
        mass.add(0);

        for (String s : combine(peptide)){
            char[] chars  = s.toCharArray();
            int sum = 0;
            for (char ch : chars){
                sum += weights.get(ch);
            }
            mass.add(sum);
        }
        Collections.sort(mass);

        return mass;
    }

    public static Integer score(String peptyde, int[] spectrum){
        int count = 0;
        List<Integer> cyclocpec = cyclospectrum(peptyde);
        int i = 0;
        int j = 0;
        while (i < spectrum.length && j < cyclocpec.size()){
            if (spectrum[i] == cyclocpec.get(j)){
                count++;
                i++;
                j++;
            }else if (spectrum[i] > cyclocpec.get(j)){
                j++;
            }else {
                i++;
            }
        }

        return count;
    }
}
