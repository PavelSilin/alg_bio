package ba4i;

import java.util.*;

/**
 * @author Anna Osipova Anna_Osipova@epam.com
 */

public class LeaderBoardBuilder {

    private Map<Character, Integer> weights;

    private int[] spectrum;
    private int maxSizeOfBoard;
    int parentMass;

    private List<String> leaderBoard;
    String leaderPeptide;
    int leaderScore;

    public LeaderBoardBuilder(int[] spectrum, int maxSizeOfBoard){
        this.spectrum = spectrum;
        this.maxSizeOfBoard = maxSizeOfBoard;
        this.weights = Amino.aminoMassesMap();
        this.leaderBoard = new ArrayList<>();
    }

    public LeaderBoardBuilder(Map<Character, Integer> weights, int[] spectrum, int maxSizeOfBoard){
        this.spectrum = spectrum;
        this.maxSizeOfBoard = maxSizeOfBoard;
        this.leaderBoard = new ArrayList<>();
        this.weights = weights;
    }

    public String leaderBoardCyclopeptideSequencing(){
        initLeaderBoard();

        while (!leaderBoard.isEmpty()){
            searchLeaderPeptide();
        }

        return peptideToMassesView(leaderPeptide);
    }

    private void searchLeaderPeptide() {
        leaderBoard = expand();
        List<String> newBoard = new ArrayList<>(leaderBoard);
        for (String peptide : leaderBoard) {
            int mass = calcMasses(peptide);
            if (mass == parentMass){
                Integer score = TheoreticalSpectrum.score(peptide, spectrum);
                if (score > leaderScore){
                    leaderPeptide = peptide;
                    leaderScore = score;
                }
            }else if (mass > parentMass) {
                newBoard.remove(peptide);
            }
        }
        leaderBoard = newBoard;
        cutLeaderBoard();
    }

    private void initLeaderBoard() {
        leaderPeptide = "";
        leaderScore = 0;
        leaderBoard.add(leaderPeptide);
        Arrays.sort(spectrum);
        parentMass = spectrum[spectrum.length - 1];
    }

    private List<String> expand(){
        List<String> expandBoard = new ArrayList<>();
        for (String peptide : leaderBoard){
            for (char amino : weights.keySet()){
                expandBoard.add(peptide + amino);
            }
        }
        return expandBoard;
    }

    private int calcMasses(String peptide){
        if (peptide.length() == 0) return 0;

        int sum = 0;
        for (char ch : peptide.toCharArray()){
            Integer mass = weights.get(ch);
            if(mass != null) {
                sum += mass;
            }
        }
        return sum;
    }

    private void cutLeaderBoard(){
        if (leaderBoard.size() < maxSizeOfBoard) return;

        List<String> newBoard = new ArrayList<>();
        Collections.sort(leaderBoard, (o1, o2) -> - TheoreticalSpectrum.score(o1,spectrum).compareTo(TheoreticalSpectrum.score(o2,spectrum)));

        int nscore = TheoreticalSpectrum.score(leaderBoard.get(maxSizeOfBoard -1),spectrum);
        for (int i = 0; i < leaderBoard.size(); i++) {
            newBoard.add(leaderBoard.get(i));
            if (i > maxSizeOfBoard && nscore != TheoreticalSpectrum.score(leaderBoard.get(i), spectrum)) {
                break;
            }
        }

        leaderBoard = newBoard;
    }

    private String peptideToMassesView(String peptide){
        String res = "";
        char[] chars = peptide.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            res += weights.get(chars[i])+"-";
        }
        if (Objects.equals(res, "")){
            return "";
        }
        return res.substring(0, res.length() - 1);
    }
}