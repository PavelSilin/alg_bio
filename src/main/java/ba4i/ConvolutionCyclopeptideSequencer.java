package ba4i;

import java.util.*;

/**
 * Created by pavelsilin on 2/14/16.
 */
public class ConvolutionCyclopeptideSequencer {

    public static final int UP_BOUND_OF_AMINO_MASS = 200;
    int n, m;
    int[] spectrum;
    Map<Integer, Integer> frequencyOnSpectrum;
    List<Integer[]> spectrumSet;

    public ConvolutionCyclopeptideSequencer(int n, int m, int[] spectrum) {
        this.n = n;
        this.m = m;
        this.spectrum = spectrum;
    }

//    class SpectrumElement {
//
//        int count;
//        int amino;
//
//        SpectrumElement(int count, int amino){
//            this.amino = amino;
//            this.count = count;
//        }
//    }

    private int[] getCyclopeptideAlphabet(){
        frequencyOnSpectrum = fillMapOfFrequency();
        spectrumSet = getSpectrumSet();

        //sort for create top chart
        Collections.sort(spectrumSet, (o1, o2) -> o2[0] - o1[0]);
        return cropAlphabetFromSpectrumSet();
    }

    private Map<Integer, Integer> fillMapOfFrequency() {
        Arrays.sort(this.spectrum);
        Map<Integer, Integer> frequencyOnSpectrum = new HashMap<>();
        for (int i = 0; i < spectrum.length; i++) {
            for (int j = 0; j < i; j++) {
                int difference = spectrum[i] - spectrum[j];
                if (difference < UP_BOUND_OF_AMINO_MASS) {
                    Integer count = frequencyOnSpectrum.get(difference);
                    if (count == null) {
                        frequencyOnSpectrum.put(difference, 1);
                    } else {
                        frequencyOnSpectrum.put(difference, count + 1);
                    }
                }
            }
        }
        return frequencyOnSpectrum;
    }

    private List<Integer[]> getSpectrumSet() {
        List<Integer[]> spectrumSet = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : frequencyOnSpectrum.entrySet()) {
            spectrumSet.add(new Integer[]{entry.getValue(), entry.getKey()});
        }
        return spectrumSet;
    }

    private int[] cropAlphabetFromSpectrumSet() {
        int[] alphabet = new int[m];
        for (int i = 0; i < alphabet.length; i++) {
            alphabet[i] = spectrumSet.get(i)[1];
        }
        return alphabet;
    }

    public String findCyclopeptid() {
            ConvolutionCyclopeptideSequencer ccs = new ConvolutionCyclopeptideSequencer(m, n, spectrum);
            int[] alphabet = ccs.getCyclopeptideAlphabet();
            LeaderBoardBuilder lbb = new LeaderBoardBuilder(Amino.generateAminoMap(alphabet), spectrum, n);
            return lbb.leaderBoardCyclopeptideSequencing();
    }
}
