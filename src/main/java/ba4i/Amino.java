package ba4i;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Anna Osipova Anna_Osipova@epam.com
 */
public class Amino {
    private static char[] aminos = {'G', 'A', 'S', 'P', 'V', 'T', 'C', 'I', 'L', 'N', 'D', 'K', 'Q', 'E', 'M', 'H','F', 'R', 'Y', 'W'};
    private static int[] masses = {57, 71, 87, 97, 99, 101, 103, 113, 113, 114, 115, 128, 128, 129, 131, 137, 147, 156, 163, 186};

    public static Map<Character, Integer> aminoMassesMap(){
        Map<Character, Integer> masses = new HashMap<>();

        for (int i = 0; i < aminos.length; i++){
            masses.put(aminos[i], Amino.masses[i]);
        }

        return masses;
    }

    public static Map<Integer, Character> massesAminoMap(){
        Map<Integer, Character> masses = new HashMap<>();

        for (int i = 0; i < aminos.length; i++){
            masses.put(Amino.masses[i], aminos[i]);
        }

        return masses;
    }

    public static Map<Character, Integer> generateAminoMap(int[] alphabet){
        Map<Character, Integer> masses = new HashMap<>();
        Map<Integer, Character> ref = Amino.massesAminoMap();

        for (int i = 0; i < alphabet.length; i++){
            Character amino = ref.get(alphabet[i]);
            if (amino != null) {
                masses.put(amino, alphabet[i]);
            }
        }

        return masses;
    }

    public static int[] masses(){
        return masses;
    }

    public static char[] aminos(){
        return aminos;
    }
}
