package ba1a;

import util.Util;

/**
 * Created by pavelsilin on 1/19/16.
 */
public class Client {
    public static void main(String[] args) {
        String seq = args[0];
        String pattern = args[1];

        System.out.println(Util.findNumberOfOccurrencesInString(seq, pattern));
    }
}
