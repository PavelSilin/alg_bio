package ba6j;

import util.Edge;

import java.util.*;

/**
 * @author Pavel Silin, silinpavel94@gmail.com
 */
public class TwoBreakerOnGenomeGraph {

    private Map<Integer, Edge> colourMap;

    public TwoBreakerOnGenomeGraph(Collection<Edge> genomeGraph) {
        buildColourEdges(genomeGraph);
    }

    private void buildColourEdges(Collection<Edge> genomeGraph) {
        colourMap = new HashMap<>();
        for (Edge edge: genomeGraph) {
            colourMap.put(edge.getFrom(), edge);
        }
    }

    public Map<Integer, Edge> getColourMap() {
        return colourMap;
    }

    public void doTwoBreak(List<Integer> indices) {
        List<Edge> targetEdges = new ArrayList<>(2);

        for (Integer index: indices) {
            Edge edge = colourMap.remove(index);
            if (edge != null) {
                targetEdges.add(edge);
            }
        }

        colourMap.put(
                targetEdges.get(0).getFrom(),
                new Edge(targetEdges.get(0).getFrom(), targetEdges.get(1).getTo())
        );
        colourMap.put(
                targetEdges.get(1).getFrom(),
                new Edge(targetEdges.get(1).getFrom(), targetEdges.get(0).getTo())
        );
    }

    public void printGenomeGraph() {
        for (Edge edge: colourMap.values()) {
            System.out.print(", (" + edge.getFrom() + ", " + edge.getTo() + ")");
        }
    }

}
