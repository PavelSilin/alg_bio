package ba2c;

import util.Util;

/**
 * Created by pavelsilin on 1/19/16.
 */
public class ProfileMostProbableKmer {

    double[][] profile;
    String seq;
    int kmerLen;

    public ProfileMostProbableKmer(String seq, int kmerLen, double[][] profile){
        this.profile = profile;
        this.kmerLen = kmerLen;
        this.seq = seq;
    }

    public String findTheMostProbableKmer() {
        double maxProbability = 0;
        String mostProbableKmer = null;
        for (int i = 0; i < seq.length() - kmerLen + 1; i++) {
            String kmer = seq.substring(i, i + kmerLen);
            double probability = Util.calculateProbability(kmer, profile);
            if (probability > maxProbability){
                maxProbability = probability;
                mostProbableKmer = kmer;
            }
        }
        return mostProbableKmer;
    }

}
