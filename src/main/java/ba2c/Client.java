package ba2c;

import util.Util;

/**
 * Created by pavelsilin on 1/19/16.
 */
public class Client {

    public static void main(String[] args) {
        String seq = args[0];
        int kmerLen = Integer.valueOf(args[1]);
        int rowLenProfile = (args.length - 2) / Util.alphabet.length;
        double[][] profile = new double[Util.alphabet.length][rowLenProfile];

        for (int i = 0; i < Util.alphabet.length; i++) {
            for (int j = 0; j < rowLenProfile; j++) {
                profile[i][j] = Double.valueOf(args[(j + 2) + i * rowLenProfile]);
            }
        }

        ProfileMostProbableKmer profileMostProbableKmer = new ProfileMostProbableKmer(seq, kmerLen, profile);
        System.out.println(profileMostProbableKmer.findTheMostProbableKmer());

    }

}
