package ba5d;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by pavelsilin on 2/18/16.
 */
public class LongestPathDAGFinder {
    int[][] graph;
    HashMap<Integer, List<int[]>> graphMap;
    List<Integer> path;
    int remoteNode;

    public LongestPathDAGFinder(int[][] graph) {
        this.graph = graph;
        this.graphMap = fillGraphMap();
        path = new ArrayList<>();
    }

    private HashMap<Integer, List<int[]>> fillGraphMap() {
        HashMap<Integer, List<int[]>> graphMap = new HashMap<>();
        for (int[] edge : graph) {
            int node = edge[1];
            List<int[]> edges = graphMap.get(node);
            if (edges == null) {
                edges = new ArrayList<>();
                graphMap.put(node, edges);
            }
            edges.add(new int[]{edge[0], edge[2]});
        }
        return graphMap;
    }

    public int findLenghtOfLongestPath(int start, int end) {
        if (start == end){
            return 0;
        }
        List<int[]> variableForPath = graphMap.get(end);
        int maxLen = Integer.MIN_VALUE;
        if (variableForPath == null){
            return  maxLen;
        }
        for (int[] edge: variableForPath) {
            int currentLen = findLenghtOfLongestPath(start, edge[0]) + edge[1];
            if (currentLen > 0 && maxLen < currentLen){
                maxLen = currentLen;
                remoteNode = edge[0];
            }
        }
        return maxLen;
    }

    public List<Integer> reconstructPath (int start, int end) {
        List<Integer> path = new ArrayList<>();
        while (true) {
           findLenghtOfLongestPath(start, end);
           path.add(end);
           if (end == start){
               break;
           }
           end = remoteNode;
        }
        return path;
    }
}
