package ba2d;

import java.util.ArrayList;

/**
 * Created by pavelsilin on 1/17/16.
 */
public class Client {

    public static void main(String[] args) {

        int kmerLen = Integer.valueOf(args[0]);
        int numOfSeq =  Integer.valueOf(args[1]);
        String[] dna = new String[numOfSeq];
        for (int i = 2; i < args.length; i++) {
            dna[i - 2] = args[i];
        }

        GreedyMotifSearch greedyMotifSearch = new GreedyMotifSearch(kmerLen, numOfSeq, dna);
        ArrayList<String> bestMotifs = greedyMotifSearch.getBestMotifs();
        System.out.println("Motifs: ");
        for (String motif: bestMotifs) {
            System.out.println(motif);
        }
    }

}
