package ba2d;

import util.Util;

import java.util.ArrayList;

/**
 * Created by pavelsilin on 1/17/16.
 */
public class GreedyMotifSearch {

    private String[] dna;
    private int numOfSeq;
    private int kmerLen;
    private ArrayList<String> bestMotifs;
    private int bestScore;

    public GreedyMotifSearch(int kmerLen, int numOfSeq, String[] dna){
        this.bestScore = Integer.MAX_VALUE;
        this.kmerLen = kmerLen;
        this.numOfSeq = numOfSeq;
        this.dna = dna;
        this.bestMotifs = new ArrayList<>();

        greedyMotifSearch();
    }

    public ArrayList<String> getBestMotifs(){
        return bestMotifs;
    }

    private void greedyMotifSearch() {
        for (int i = 0; i < dna.length; i++) {
            bestMotifs.add(dna[i].substring(0, kmerLen));
        }

        for (int i = 0; i < dna[0].length() - kmerLen + 1; i++) {
            ArrayList<String> motifs = new ArrayList<>();
            motifs.add(dna[0].substring(i, i + kmerLen));
            for (int j = 1; j < numOfSeq; j++) {
                double[][] profile = Util.generateProfile(motifs);
                motifs.add(searchMostProbableKmer(dna[j], profile));
            }
            int score = Util.score(motifs);
            if (score < bestScore){
                bestScore = score;
                bestMotifs = motifs;
            }
        }
    }

    private String searchMostProbableKmer(String seq, double[][] profile) {
        String mostProbableKmer = null;
        double lastProbable = -1;
        for (int i = 0; i < seq.length() - kmerLen + 1; i++) {
            String currentKmer = seq.substring(i, i + kmerLen);
            double probable = calcProbable(currentKmer, profile);
            if (probable > lastProbable){
                mostProbableKmer = currentKmer;
                lastProbable = probable;
                if (probable == 1){
                    break;
                }
            }
        }
        return mostProbableKmer;
    }

    private double calcProbable(String currentKmer, double[][] profile) {
        double probable = 1;
        for (int i = 0; i < currentKmer.length(); i++) {
            switch (currentKmer.charAt(i)){
                case 'A':
                    probable = probable * profile[0][i];
                    break;
                case 'C':
                    probable = probable * profile[1][i];
                    break;
                case 'G':
                    probable = probable * profile[2][i];
                    break;
                case 'T':
                    probable = probable * profile[3][i];
                    break;
            }
        }
        return probable;
    }

}
