package gc;

/**
 * Created by pavelsilin on 1/24/16.
 */
public class Client {
    public static void main(String[] args) {
        MaxGCContentSearcher maxGCContentSearcher = new MaxGCContentSearcher(args);
        maxGCContentSearcher.calculateGCContent();

        System.out.println(maxGCContentSearcher.getMaxGCContantSeqName());
        System.out.println(maxGCContentSearcher.getMaxGCContent());
    }
}
