package gc;

import util.FastaFormat;
import util.Util;

/**
 * Created by pavelsilin on 1/24/16.
 */
public class MaxGCContentSearcher {

    private FastaFormat fastaReader;
    private String maxGCContantSeqName;
    private double maxGCContent = 0;

    public MaxGCContentSearcher(String[] data){
        this.fastaReader = new FastaFormat(data);
    }

    public String getMaxGCContantSeqName(){
        return maxGCContantSeqName;
    }

    public double getMaxGCContent(){
        return maxGCContent;
    }

    public void calculateGCContent(){
        for (int i = 0; i < fastaReader.getSeqs().size(); i++) {
            double gcContent = Util.calculateGC(fastaReader.getSeqs().get(i)[1]);
            if (maxGCContent < gcContent){
                maxGCContantSeqName = fastaReader.getSeqs().get(i)[0];
                maxGCContent = gcContent;
            }
        }
    }

}
