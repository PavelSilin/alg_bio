package ba3a;

import java.util.ArrayList;

/**
 * Created by pavelsilin on 1/19/16.
 */
public class Client {

    public static void main(String[] args) {
        int kmerLen = Integer.valueOf(args[0]);
        String seq = args[1];

        GeneratorKmerCompositionOfString generatorKmerCompositionOfString = new GeneratorKmerCompositionOfString(kmerLen, seq);
        ArrayList<String> kmerCompasition = (ArrayList<String>) generatorKmerCompositionOfString.generateKmerComposition();
        for (int i = 0; i < kmerCompasition.size(); i++) {
            System.out.println(kmerCompasition.get(i));
        }
    }

}
