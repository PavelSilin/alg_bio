package ba3a;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by pavelsilin on 1/19/16.
 */
public class GeneratorKmerCompositionOfString {

    String seq;
    int kmerLen;

    public GeneratorKmerCompositionOfString(int kmerLen, String seq){
        this.kmerLen = kmerLen;
        this.seq = seq;
    }

    public List<String> generateKmerComposition(){
        int numOfKmer = seq.length() - kmerLen + 1;
        ArrayList<String> kmerComposition = new ArrayList<>(numOfKmer);
        for (int i = 0; i < numOfKmer; i++) {
            kmerComposition.add(seq.substring(i, i +kmerLen));
        }
        Collections.sort(kmerComposition, new KmerCompositionComparator());
        return kmerComposition;
    }

    public class KmerCompositionComparator implements Comparator<String>{

        int kmerLength;

        public KmerCompositionComparator() {
            kmerLength = kmerLen;
        }

        @Override
        public int compare(String o1, String o2) {
            int minLen = Math.min(o1.length(), o2.length());
            for (int i = 0; i < minLen; i++) {
                int mod = o1.charAt(i) - o2.charAt(i);
                if (mod > 0){
                    return 1;
                }else if(mod < 0){
                    return  -1;
                }
            }
            return 0;
        }
    }
}
