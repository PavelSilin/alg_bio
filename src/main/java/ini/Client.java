package ini;

import util.Util;

/**
 * Created by pavelsilin on 1/24/16.
 */
public class Client {
    public static void main(String[] args) {
        int[] answer = Util.countBasesInSeq(args[0]);
        for (int i = 0; i < answer.length; i++) {
            System.out.print(answer[i] + " ");
        }
    }
}
