package ba1d;

import util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pavelsilin on 1/22/16.
 */
public class PatternMatchingFinder {
    
    String pattern;
    String dna;
    
    PatternMatchingFinder(String pattern, String dna){
        this.dna = dna;
        this.pattern = pattern;
    }
    
    public List<Integer> findCoordinateOfPatternMatching(){
        ArrayList<Integer> coordinate = new ArrayList<>();
        int pattenrLength = pattern.length();

        for (int i = 0; i < dna.length() - pattenrLength + 1; i++) {
            if (Util.isIdenticalSeq(dna.substring(i, i + pattenrLength), pattern)){
                coordinate.add(i);
            }
        }
        return coordinate;
    }
    
}
