package ba1d;

/**
 * Created by pavelsilin on 1/22/16.
 */
public class Client {
    public static void main(String[] args) {
        String pattern = args[0];
        String dna = args[1];

        PatternMatchingFinder patternMatchingFinder = new PatternMatchingFinder(pattern, dna);
        System.out.println(patternMatchingFinder.findCoordinateOfPatternMatching());
    }
}
