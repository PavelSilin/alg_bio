package ba3b;

import util.Util;

/**
 * Created by pavelsilin on 1/19/16.
 */
public class Client {
    public static void main(String[] args) {

        String[] kmers = new String[args.length];

        for (int i = 0; i < args.length; i++) {
            kmers[i] = args[i];
        }

        System.out.println(Util.reconstructDNA(kmers));
    }
}
