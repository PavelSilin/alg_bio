package util;

import java.util.ArrayList;

/**
 * Created by pavelsilin on 1/24/16.
 */
public class FastaFormat {

    private String[] data;
    private ArrayList<String[]> seqs;

    public FastaFormat(String[] data){
        this.data = data;
        seqs = new ArrayList<>(data.length);

        read();
    }

    public ArrayList<String[]> getSeqs(){
        return seqs;
    }

    private void read() {
        if (data.length == 0 || data == null){
            return;
        }
        StringBuilder seqBuilder = new StringBuilder();
        String seqName = null;
        for (int i = 0; i < data.length; i++) {
            if (data[i].charAt(0) == '>') {
                if (seqBuilder.length() != 0){
                    seqs.add(new String[]{seqName, seqBuilder.toString()});
                    seqBuilder = new StringBuilder();
                }
                seqName = data[i];
            }else {
                seqBuilder.append(data[i]);
            }
        }
        seqs.add(new String[]{seqName, seqBuilder.toString()});
    }


}
