package util;

/**
 * @author Pavel Silin, silinpavel94@gmail.com
 */
public class Edge {

    private int from;
    private int to;

    public Edge(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public int getTo() {
        return to;
    }

    public int getFrom() {
        return from;
    }

    @Override
    public int hashCode() {
        return from * 31 + to * 31;
    }
}