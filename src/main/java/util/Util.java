package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pavelsilin on 1/17/16.
 */
public class Util {

    static public char[] alphabet = new char[]{'A', 'C', 'G', 'T'};

    static public int score(ArrayList<String> motifs) {
        int score = 0;
        String consensusString = searchConsensusString(motifs);
        for (int i = 0; i < motifs.size(); i++) {
            String currentMotif = motifs.get(i);
            for (int j = 0; j < motifs.get(0).length(); j++) {
                if (currentMotif.charAt(j) != consensusString.charAt(j)){
                    score++;
                }
            }
        }
        return score;
    }

    static public boolean isIdenticalSeq(String seq1, String seq2){
        return seq1.equals(seq2);
    }

    static public String searchConsensusString(ArrayList<String> dna) {
        String consensusString = new String();
        int seqLen = dna.get(0).length();

        for (int i = 0; i < seqLen; i++) {
            int[] countBasesInThisPosition = countBasesInPositionForMotifs(dna, i);

            int mostProbableSymbolIndex = 0;
            int lastProbable = 0;
            for (int j = 0; j < countBasesInThisPosition.length; j++) {
                if (lastProbable < countBasesInThisPosition[j]){
                    mostProbableSymbolIndex = j;
                    lastProbable = countBasesInThisPosition[j];
                }
            }
            consensusString = consensusString + alphabet[mostProbableSymbolIndex];
        }
        return consensusString;
    }

    static public int[] countBasesInPositionForMotifs(List<String> dna, int position) {
        int[] countBasesInThisPosition = new int[alphabet.length];
        for (int j = 0; j < dna.size(); j++) {
            switch (dna.get(j).charAt(position)){
                case 'A':
                    countBasesInThisPosition[0]++;
                    break;
                case 'C':
                    countBasesInThisPosition[1]++;
                    break;
                case 'G':
                    countBasesInThisPosition[2]++;
                    break;
                case 'T':
                    countBasesInThisPosition[3]++;
                    break;
            }
        }
        return countBasesInThisPosition;
    }

    static public int[] countBasesInSeq(String seq) {
        int[] countBasesInThisPosition = new int[alphabet.length];
        for (int j = 0; j < seq.length(); j++) {
            switch (seq.charAt(j)){
                case 'A':
                    countBasesInThisPosition[0]++;
                    break;
                case 'C':
                    countBasesInThisPosition[1]++;
                    break;
                case 'G':
                    countBasesInThisPosition[2]++;
                    break;
                case 'T':
                    countBasesInThisPosition[3]++;
                    break;
            }
        }
        return countBasesInThisPosition;
    }

    static public String dnaToRna(String seq) {
        StringBuilder rna = new StringBuilder();
        for (int j = 0; j < seq.length(); j++) {
            if (seq.charAt(j) == 'T'){
                rna.append('U');
            }else {
                rna.append(seq.charAt(j));
            }
        }
        return rna.toString();
    }

    public static double[][] generateProfile(List<String> dna) {
        double[][] profile = new double[alphabet.length][dna.get(0).length()];
        int[][] pseudocounts = fillCounts(dna);

        for (int i = 0; i < pseudocounts.length; i++) {
            for (int j = 0; j < pseudocounts[0].length; j++) {
                profile[i][j] = (double) pseudocounts[i][j] / dna.size();
            }
        }
        return profile;
    }

    public static double[][] generateLaplasProfile(List<String> dna) {
        double[][] profile = new double[alphabet.length][dna.get(0).length()];
        int[][] pseudocounts = fillCounts(dna);
        pseudocounts = useLaplasRule(pseudocounts);

        for (int i = 0; i < pseudocounts.length; i++) {
            for (int j = 0; j < pseudocounts[0].length; j++) {
                profile[i][j] = ((double) pseudocounts[i][j]) / (alphabet.length + dna.size());
            }
        }
        return profile;
    }

    private static int[][] fillCounts(List<String> dna) {
        int[][] pseudocounts = new int[alphabet.length][dna.get(0).length()];
        for (int i = 0; i < dna.get(0).length(); i++) {
            int[] probableForNucleatides = Util.countBasesInPositionForMotifs(dna, i);
            for (int j = 0; j < alphabet.length; j++) {
                pseudocounts[j][i] = probableForNucleatides[j];
            }
        }
        return pseudocounts;
    }

    private static int[][] useLaplasRule(int [][] pseudocounts) {
        for (int i = 0; i < pseudocounts[0].length; i++) {
            for (int j = 0; j < alphabet.length; j++) {
                pseudocounts[j][i] = pseudocounts[j][i] + 1;
            }
        }
        return pseudocounts;
    }

    public static double calculateProbability(String seq, double[][] profile) {
        double probability = 1.0;
        for (int i = 0; i < seq.length(); i++) {
            switch (seq.charAt(i)){
                case 'A':
                    probability = probability * profile[0][i];
                    break;
                case 'C':
                    probability = probability * profile[1][i];
                    break;
                case 'G':
                    probability = probability * profile[2][i];
                    break;
                case 'T':
                    probability = probability * profile[3][i];
                    break;
            }
        }
        return probability;
    }

    public static List<String> findMostFrequentString(String seq, int kmerLen){
        HashMap<String, Integer> frequenceCount = new HashMap<>();
        ArrayList<String> maxFrequenceKmer = new ArrayList<>();
        int maxFrequent = 0;

        for (int i = 0; i < seq.length() - kmerLen + 1; i++) {
            String kmer = seq.substring(i, i + kmerLen);
            Integer count = frequenceCount.get(kmer);
            if (count == null){
                frequenceCount.put(kmer, 1);
            }else{
                frequenceCount.put(kmer, count + 1);
            }
            count = frequenceCount.get(kmer);
            if (count > maxFrequent){
                maxFrequent = count;
            }
        }

        for (Map.Entry entry: frequenceCount.entrySet()) {
            if ((int) entry.getValue() == maxFrequent){
                maxFrequenceKmer.add((String) entry.getKey());
            }
        }
        return maxFrequenceKmer;
    }

    public static String reconstructDNA(String[] kmerComposition){
        String dna = new String();
        for (int i = 0; i < kmerComposition.length - 1; i++) {
            dna = dna + kmerComposition[i].charAt(0);
        }
        dna = dna + kmerComposition[kmerComposition.length - 1];
        return dna;
    }

    public static int findNumberOfOccurrencesInString(String seq, String pattern){
        int count = 0;
        int patternLength = pattern.length();
        for (int i = 0; i < seq.length() - pattern.length() + 1; i++) {
            if (isIdenticalSeq(seq.substring(i, i + patternLength), pattern)){
                count++;
            }
        }
        return count;
    }

    public static String complementsSeq(String pattern){
        StringBuilder complementsSeq = new StringBuilder();
        for (int i = pattern.length() - 1; i >= 0; i--) {
            switch (pattern.charAt(i)){
                case 'A':
                    complementsSeq.append("T");
                    break;
                case 'C':
                    complementsSeq.append("G");
                    break;
                case 'G':
                    complementsSeq.append("C");
                    break;
                case 'T':
                    complementsSeq.append("A");
                    break;
            }
        }
        return complementsSeq.toString();
    }

    public static int hammingDistance(String seq1, String seq2) {
        int distance = 0;
            for (int j = 0; j < seq1.length(); j++){
                if (seq2.charAt(j) != seq1.charAt(j)){
                    distance++;
                }
            }
        return distance;
    }

    public static double calculateGC(String seq) {
        int gcCount = 0;
        for (int i = 0; i < seq.length(); i++) {
            if (seq.charAt(i) == 'G' || seq.charAt(i) == 'C'){
                gcCount++;
            }
        }
        return ((double) gcCount / seq.length()) * 100;
    }
}
