package ba1b;

import util.Util;

/**
 * Created by pavelsilin on 1/19/16.
 */
public class Client {

    public static void main(String[] args) {
        String seq = args[0];
        int kmerLen = Integer.valueOf(args[1]);

        System.out.println(Util.findMostFrequentString(seq, kmerLen));

    }

}
