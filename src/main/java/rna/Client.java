package rna;

import util.Util;

/**
 * Created by pavelsilin on 1/24/16.
 */
public class Client {
    public static void main(String[] args) {
        System.out.println(Util.dnaToRna(args[0]));
    }
}
