package ba3c;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Pavel_Silin on 2/3/2016.
 */
public class Client {
    public static void main(String[] args) {
        ArrayList<String> data = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File(args[0]));
            while (scanner.hasNext()){
                data.add(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        OverlapGraphConstructor overlapGraphConstructor = new OverlapGraphConstructor(data);
        overlapGraphConstructor.constructPrefixMap();
        for (ArrayList<String> overlap: overlapGraphConstructor.getOverlaps()) {
            for (int i = 1; i < overlap.size(); i++) {
                System.out.println(overlap.get(0) + " -> " + overlap.get(i));
            }
        }
    }
}
