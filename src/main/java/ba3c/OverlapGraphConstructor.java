package ba3c;

import java.util.*;

/**
 * Created by Pavel_Silin on 2/3/2016.
 */
public class OverlapGraphConstructor {

    ArrayList<String> kmers;
    HashMap<String, ArrayList<String>> prefixs;

    public OverlapGraphConstructor(ArrayList<String> kmers) {
        this.kmers = kmers;
        this.prefixs = new HashMap<>();
    }

    public void constructPrefixMap() {
        for (String kmer : kmers) {
            ArrayList<String> overlap = prefixs.get(kmer.substring(0, kmer.length() - 1));
            if (overlap == null) {
                overlap = new ArrayList<>();
                prefixs.put(kmer.substring(0, kmer.length() - 1), overlap);
            }
            overlap.add(kmer);
        }
    }

    public TreeSet<ArrayList<String>> getOverlaps() {
        TreeSet<ArrayList<String>> overlaps = new TreeSet<>(new KmerCompositionComparator());
        for (String sufix: kmers) {
            ArrayList<String> OverlapsForOneKmer = new ArrayList<>();
            OverlapsForOneKmer.add(sufix);
            if (prefixs.get(sufix.substring(1)) != null){
                OverlapsForOneKmer.addAll(prefixs.get(sufix.substring(1)));
            }
            overlaps.add(OverlapsForOneKmer);
        }
        return overlaps;
    }

    public class KmerCompositionComparator implements Comparator<ArrayList<String>>{

        @Override
        public int compare(ArrayList<String> o1, ArrayList<String> o2) {
            String s1 = o1.get(0);
            String s2 = o2.get(0);
            int minLen = Math.min(s1.length(), s2.length());
            for (int i = 0; i < minLen; i++) {
                int mod = s1.charAt(i) - s2.charAt(i);
                if (mod > 0){
                    return 1;
                }else if(mod < 0){
                    return  -1;
                }
            }
            return 0;
        }
    }

}
