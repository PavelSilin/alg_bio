package ba2g;

import java.util.ArrayList;

/**
 * Created by pavelsilin on 1/17/16.
 */
public class Client {

    public static void main(String[] args) {
        int kmerLen = Integer.valueOf(args[0]);
        int numOfSeq =  Integer.valueOf(args[1]);
        int numOfIter =  Integer.valueOf(args[2]);
        String[] dna = new String[numOfSeq];
        for (int i = 3; i < args.length; i++) {
            dna[i - 3] = args[i];
        }


        ArrayList<String> bestMotifs = null;
        int bestScore = Integer.MAX_VALUE;
        for (int i = 0; i < 100; i++) {
            GibbsSampler gibbsSampler = new GibbsSampler(kmerLen, numOfSeq, numOfIter, dna);
            gibbsSampler.gibbsMotifSearch();
            int score = gibbsSampler.getBestScore();
            if (score < bestScore){
                bestScore = score;
                bestMotifs = gibbsSampler.getBestMotifs();
            }
        }

        System.out.println("Best score: " + bestScore);
        System.out.println("Best motifs: ");
        bestMotifs.forEach(System.out::println);

    }

}
