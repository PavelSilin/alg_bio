package ba2g;

import util.Util;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by pavelsilin on 1/17/16.
 */
public class GibbsSampler {

    private String[] dna;
    private int kmerLen;
    private int numberOfSeq;
    private int numberOfIteration;
    private ArrayList<String> bestMotifs;
    private Random random;
    private int bestScore;

    public GibbsSampler(int kmerLen, int numberOfSeq, int numberOfIteration, String[] dna){
        this.bestScore = Integer.MAX_VALUE;
        this.kmerLen = kmerLen;
        this.numberOfSeq = numberOfSeq;
        this.numberOfIteration = numberOfIteration;
        this.dna = dna;
        this.random = new Random();
    }

    public ArrayList<String> getBestMotifs(){
        return bestMotifs;
    }
    public int getBestScore(){
        return bestScore;
    }

    public void gibbsMotifSearch() {
        ArrayList<String> motifs = new ArrayList<>(numberOfSeq);
        for (String seq : dna) {
            int beginMotifIndex = random.nextInt(seq.length() - kmerLen + 1);
            motifs.add(seq.substring(beginMotifIndex, beginMotifIndex + kmerLen));
        }
        bestMotifs = motifs;

        for (int i = 0; i < numberOfIteration; i++){
            int indexOfSelectRow = random.nextInt(numberOfSeq);
            double[][] profile = Util.generateLaplasProfile(discardSelectRow(motifs, indexOfSelectRow));
            motifs.set(indexOfSelectRow, selectRandomlyMotif(dna[indexOfSelectRow], profile));
            int currentScore = Util.score(motifs);
            if (currentScore < bestScore) {
                bestScore = currentScore;
                bestMotifs = motifs;
            }
        }
    }

    private String selectRandomlyMotif(String s, double[][] profile) {
        double[] probability = new double[s.length() - kmerLen + 1];
        for (int i = 0; i < probability.length; i++) {
            probability[i] = Util.calculateProbability(s.substring(i, i + kmerLen), profile);
        }
        int indexOfRandomMotif = getRandomMotifIndex(probability);
        return s.substring(indexOfRandomMotif, indexOfRandomMotif + kmerLen);
    }

    private int getRandomMotifIndex(double[] probability) {
        double[] motifsBounds = new double[probability.length];
        double upperBound = 0;
        double borderOfLastMotif = 0;

        for (int i = 0; i < probability.length; i++) {
            double borderOfMotif = probability[i];
            motifsBounds[i] = borderOfMotif + borderOfLastMotif;
            upperBound += borderOfMotif;
            borderOfLastMotif = motifsBounds[i];
        }

        double randomPoint = random.nextDouble() * upperBound;
        for (int i = 0; i < motifsBounds.length; i++) {
            if (motifsBounds[i] >= randomPoint){
                return i;
            }
        }

        return motifsBounds.length - 1;
    }

    private ArrayList<String> discardSelectRow(ArrayList<String> motifs, int indexOfSelectRow) {
        ArrayList<String> motifsWithOutSelectRow = (ArrayList<String>) motifs.clone();
        motifsWithOutSelectRow.remove(indexOfSelectRow);
        return motifsWithOutSelectRow;
    }

}
