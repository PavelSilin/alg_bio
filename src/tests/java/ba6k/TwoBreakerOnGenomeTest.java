package ba6k;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * @author Pavel Silin, silinpavel94@gmail.com
 */

@RunWith(value = Parameterized.class)
public class TwoBreakerOnGenomeTest {
    public static final String refFiles = "/home/pavelsilin/git/alg_bio/src/tests/resources/";
    private final String file;
    private String refAnswer;
    private String answer;


    public TwoBreakerOnGenomeTest(String file, String refAnswer){
        this.refAnswer = refAnswer;
        this.file = file;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{
                {refFiles + "test_ba6k", "(+65 -66 -67 -68 +69 +70 -1 -2 -3 +4 -5 -6 +7 -8 -24 -25 +26 +27 +28 +29 +30" +
                        " -31 -32 +33 -34 +35 +36 -37 +38 +39 +40 +41 -42 +43 +44 +45 -46 +47 +48 +49 +50 +51 +52 +53" +
                        " -54 -55 -56 +57 -58 +59 -60 +61 +62 +63 +64 +64)\n" +
                        "(+9 +10 -11 -12 +13 +14 -15 -16 -17 +18 +19 -20 +21 -22 -23)\n"}
        };
        return Arrays.asList(data);
    }

    @Before
    public void init() {
        try {
            Scanner scanner = new Scanner(new File(file));
            String graphString = scanner.nextLine();
            String nodesForSwap = scanner.nextLine();
            List<Integer> graph = new ArrayList<>();
            graphString = graphString.replaceAll("\\)", "").replaceAll("\\(", "");
            String[] edges = graphString.split(" ");
            for (String edge: edges) {
                graph.add(Integer.parseInt(edge));
            }
            String[] swapNodes = nodesForSwap.split(", ");

            TwoBreakerOnGenome genomeBreaker = new TwoBreakerOnGenome(graph);
            genomeBreaker.doTwoBreak(Integer.parseInt(swapNodes[0]), Integer.parseInt(swapNodes[1]),
                    Integer.parseInt(swapNodes[2]), Integer.parseInt(swapNodes[3]));
            genomeBreaker.constructBrokenGenomes();

            StringBuilder builder = new StringBuilder();
            for (List<Integer> genome: genomeBreaker.getBrokenGenomes()) {
                builder.append("(");
                for (int i = 0; i < genome.size() - 2; i += 2) {
                    int node1 = genome.get(i);
                    int node2 = genome.get(i + 1);
                    if (node2 - node1 > 0){
                        builder.append("+" + ((node2 / 2)) + " ");
                    }else {
                        builder.append("-" + ((node1 / 2)) + " ");
                    }
                }
                int node1 = genome.get(genome.size() - 2);
                int node2 = genome.get(genome.size() - 1);
                if (node2 - node1 > 0){
                    builder.append("+" + ((node2 / 2)));
                }else {
                    builder.append("-" + ((node1 / 2)));
                }
                builder.append(")\n");
                answer = builder.toString();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void run(){
        System.out.print(answer);
        Assert.assertEquals(answer, refAnswer);
    }
}