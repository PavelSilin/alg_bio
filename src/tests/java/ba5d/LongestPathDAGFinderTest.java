package ba5d;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by pavelsilin on 2/21/16.
 */

@RunWith(value = Parameterized.class)
public class LongestPathDAGFinderTest {

    public static final String refFiles = "/home/pavelsilin/git/alg_bio/src/tests/resources/";
    private final String file;
    private String refAnswer;
    private String currentAnswer;

    public LongestPathDAGFinderTest(String file, String refAnswer){
        this.refAnswer = refAnswer;
        this.file = file;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{
                {refFiles + "test_ba5d_extra", "137\n1->12->16->31"},
                {refFiles + "test_ba5d_small", "9\n0->2->3->4"}
        };
        return Arrays.asList(data);
    }

    @Before
    public void init() {
        try {
            Scanner scanner = new Scanner(new File(file));
            int from = scanner.nextInt();
            scanner.nextLine();
            int to = scanner.nextInt();
            scanner.nextLine();
            ArrayList<int[]> edges = new ArrayList<>();
            while (scanner.hasNext()) {
                String[] edge = scanner.nextLine().split("[->:]");
                edges.add(new int[]{Integer.parseInt(edge[0]), Integer.parseInt(edge[2]), Integer.parseInt(edge[3])});
            }
            int[][] graph = new int[edges.size()][];
            for (int i = 0; i < edges.size(); i++) {
                graph[i] = edges.get(i);
            }
            LongestPathDAGFinder finder = new LongestPathDAGFinder(graph);
            int longestPathLenght = finder.findLenghtOfLongestPath(from, to);
            List<Integer> longestPath = finder.reconstructPath(from, to);
            currentAnswer = buildAnswer(longestPathLenght, longestPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private String buildAnswer(int longestPathLenght, List<Integer> longestPath) {
        StringBuilder builder = new StringBuilder();
        builder.append(Integer.toString(longestPathLenght) + '\n');
        for (int i = longestPath.size() - 1; i > 0; i--) {
            builder.append(longestPath.get(i) + "->");
        }
        builder.append(longestPath.get(0));
        return builder.toString();
    }

    @Test
    public void SummaryTest(){
        Assert.assertEquals(refAnswer, currentAnswer);
    }

}