package ba4i;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Scanner;

/**
 * Created by pavelsilin on 2/18/16.
 */

@RunWith(value = Parameterized.class)
public class ConvolutionCyclopeptideSequencerTest {

    private final String file;
    private String refAnswer;
    private String currentAnswer;

    public ConvolutionCyclopeptideSequencerTest(String file, String refAnswer){
        this.refAnswer = refAnswer;
        this.file = file;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{
                {ConvolutionCyclopeptideSequencerTest.class.getResource("test").getPath(), "113-115-114-128-97-163-131-129-129-147-57-57-129"}
        };
        return Arrays.asList(data);
    }

    @Before
    public void init() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(file));
            int maxSizeAlphabet = scanner.nextInt();
            int maxSizeTopTable = scanner.nextInt();

            //cut  '\n'
            scanner.nextLine();

            String[] s = scanner.nextLine().split(" ");
            scanner.close();

            int[] spectrum = new int[s.length];
            for (int i = 0; i < s.length; i++) {
                spectrum[i] = Integer.parseInt(s[i]);
            }
            ConvolutionCyclopeptideSequencer sequencer = new ConvolutionCyclopeptideSequencer(maxSizeAlphabet, maxSizeTopTable, spectrum);
            currentAnswer = sequencer.findCyclopeptid();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void SummaryTest(){
        Assert.assertEquals(refAnswer, currentAnswer);
    }

}