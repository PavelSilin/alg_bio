package ba6j;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.Edge;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * @author Pavel Silin, silinpavel94@gmail.com
 */

@RunWith(value = Parameterized.class)
public class TwoBreakerOnGenomeGraphTest {

    public static final String refFiles = "/home/pavelsilin/git/alg_bio/src/tests/resources/";
    private final String file;
    private String refAnswer;

    public TwoBreakerOnGenomeGraphTest(String file, String refAnswer){
        this.refAnswer = refAnswer;
        this.file = file;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{
                {refFiles + "test_ba6j_small", "137\n1->12->16->31"},
        };
        return Arrays.asList(data);
    }

    @Before
    public void init() {
        try {
            Scanner scanner = new Scanner(new File(file));
            String graphString = scanner.nextLine();
            String nodesForSwap = scanner.nextLine();
            List<Edge> graph = new ArrayList<>();

            String[] edges = graphString.split("\\), ");
            for (String edge: edges) {
                edge = edge.replaceAll("\\(", "").replaceAll("\\)", "");
                String[] nodesInEdges = edge.split(", ");
                graph.add(new Edge(Integer.parseInt(nodesInEdges[0]), Integer.parseInt(nodesInEdges[1])));
            }
            String[] swapNodes = nodesForSwap.split(", ");

            TwoBreakerOnGenomeGraph genomeGraphBreaker = new TwoBreakerOnGenomeGraph(graph);
            List<Integer> indices = new ArrayList<>();
            indices.add(Integer.parseInt(swapNodes[0]));
            indices.add(Integer.parseInt(swapNodes[1]));
            indices.add(Integer.parseInt(swapNodes[2]));
            indices.add(Integer.parseInt(swapNodes[3]));

            genomeGraphBreaker.doTwoBreak(indices);
            genomeGraphBreaker.printGenomeGraph();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void run(){}


}